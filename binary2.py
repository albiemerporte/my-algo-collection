
binary = '000111'
binary = binary[::-1]
power = 8

result = 0
i = len(binary)

for i in range(len(binary) - 1, -1, -1):
    result += (int(binary[i]) * power ** i)
    print("This is binary[len(binary)]", binary[i], "| This is power", power, " | this is [i] high num at top :", i)
    i -= 1

print("The Value of {} is {}".format(binary[::-1], result))
