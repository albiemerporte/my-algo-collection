# octal digit read as 3 digits in python3
# 0000 - 000
# 0001 - 001    when add 001 from other group of digits
# 0010 - 002      it becomes 010
# 0011 - 003
# 0100 - 004
#    "
#10100 - 024

#example
# 001 + 100 = 24

binary_number = '000100'
decimal_number = int(binary_number, 2)
octal_number = oct(decimal_number)[2:]  # Remove the '0o' prefix

print(octal_number)
